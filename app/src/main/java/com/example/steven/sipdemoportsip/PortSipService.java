package com.example.steven.sipdemoportsip;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


public class PortSipService extends Service {
	static PortSipService instance;
	private MainActivity myApplication;
	public PortSipService() {
		super();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		myApplication = (MainActivity) getApplicationContext();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Ring.getInstance(this).stop();
		myApplication = null;
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
