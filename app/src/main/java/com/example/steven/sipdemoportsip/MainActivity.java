package com.example.steven.sipdemoportsip;

import com.portsip.OnPortSIPEvent;
import com.portsip.PortSipEnumDefine;
import com.portsip.PortSipErrorcode;
import com.portsip.PortSipSdk;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.os.PowerManager;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements OnPortSIPEvent {

    Intent srvIntent = null;
    PortSipSdk mSipSdk;
    String LogPath = null;
    Button call;
    TextView statusText;
    String callTo;
    EditText callee;
    Line line;
    Line line1;
    Button speech;
    Button hangup;

    private final int SPEECH_RECOGNITION_CODE = 1;
    private PowerManager.WakeLock mCpuLock;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusText = (TextView) findViewById(R.id.status_text);
        call = (Button) findViewById(R.id.call_button);
        callee = (EditText) findViewById(R.id.callee_text);
        speech = (Button) findViewById(R.id.call_speech);
        hangup = (Button) findViewById(R.id.hangup_button);

        line = new Line(0);
        mSipSdk = new PortSipSdk();

        srvIntent = new Intent(this, PortSipService.class);
        this.startService(srvIntent);

        mSipSdk.setOnPortSIPEvent(this);

        int result = initializeSip();
        if(result == PortSipErrorcode.ECoreErrorNone) {
            statusText.setText("success");
            result = mSipSdk.registerServer(90, 3);
            if(result!=PortSipErrorcode.ECoreErrorNone ){
                statusText.setText("success - failed");
            }
        } else {
            statusText.setText("failed");
        }
        mSipSdk.setLoudspeakerStatus(false);

        mSipSdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G729);
        if (mSipSdk.isAudioCodecEmpty()) {
            Toast.makeText(getApplicationContext(), "Audio Codec Empty,add audio codec at first", Toast.LENGTH_SHORT).show();
        }

       // callTo = callee.getText().toString();
        call.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                long sessionId = mSipSdk.call(callee.getText().toString(), false, false);
                line.setSessionId(sessionId);
                line.setSessionState(true);
                line.setVideoState(false);
                if (sessionId <= 0) {
                    Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        speech.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startSpeechToText();
            }
        });

        hangup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hangupCall();
                statusText.setText("call ended");
            }
        });
    }
    private void hangupCall() {
        Ring.getInstance(getApplicationContext()).stopRingTone();
        Ring.getInstance(getApplicationContext()).stopRingBackTone();
        if (line.getRecvCallState()) {
            mSipSdk.rejectCall(line.getSessionId(), 486);
            line.reset();
            return;
        }

        if (line.getSessionState()) {
            mSipSdk.hangUp(line.getSessionId());
            line.reset();
        }
    }
    private void startSpeechToText() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speak something...");
        try {
            startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Speech recognition is not supported in this device.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SPEECH_RECOGNITION_CODE: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = result.get(0);
                    String resultx = "";
                    Boolean isCallable = true;
                    if(text.equalsIgnoreCase("call Matt")) {
                        resultx = "0401950967";
                    }else if (text.equalsIgnoreCase("call support")) {
                        resultx = "810";
                    } else {
                        isCallable = false;
                        callee.setText("unrecognized");
                    }
                    if(isCallable) {
                        callee.setText(resultx);
                        long sessionId = mSipSdk.call(callee.getText().toString(), false, false);
                        if (sessionId <= 0) {
                            Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            }
        }
    }
    private int initializeSip() {
        UserInfo info = new UserInfo();
        String localIP = "0.0.0.0";
        int localPort = new Random().nextInt(4940) + 5060;
        LogPath = Environment.getExternalStorageDirectory().getAbsolutePath() + '/';
        mSipSdk.CreateCallManager(getApplicationContext());
        int result = mSipSdk.initialize(info.getTransType(),localIP, localPort,
                PortSipEnumDefine.ENUM_LOG_LEVEL_NONE, LogPath,
                1, "PortSIP VoIP SDK for Android",
                0,0);
        if (result != PortSipErrorcode.ECoreErrorNone) {
            Toast.makeText(this, "failed step 2", Toast.LENGTH_SHORT).show();
            return result;
        }
        mSipSdk.setSrtpPolicy(info.getSrtpType());
        mSipSdk.setLicenseKey("PORTSIP_TEST_LICENSE");// step 3
        result = mSipSdk.setUser(info.getUserName(),info.getUserDisplayName(),info.getAuthName(),info.getUserPassword(),
                info.getUserdomain(),info.getSipServer(),info.getSipPort(),
                info.getStunServer(), info.getStunPort(), null, 5060);// step 4
        if (result != PortSipErrorcode.ECoreErrorNone) {
            Toast.makeText(this, "failed step 4", Toast.LENGTH_SHORT).show();
            return result;
        }
        SettingConfig.setAVArguments(getApplicationContext(),mSipSdk);
        return PortSipErrorcode.ECoreErrorNone;
    }


    public int answerSessionCall(Line sessionLine, boolean videoCall){
        long sessionId = sessionLine.getSessionId();
        int rt = PortSipErrorcode.INVALID_SESSION_ID;
        Ring.getInstance(this).stopRingTone();
        if(sessionId != PortSipErrorcode.INVALID_SESSION_ID) {
            rt = mSipSdk.answerCall(sessionLine.getSessionId(), videoCall);
        }
        if(rt == 0){
            sessionLine.setSessionState(true);
           // setCurrentLine(sessionLine);
            if(videoCall) {
                sessionLine.setVideoState(true);
            }else{
                sessionLine.setVideoState(false);
            }
          //  updateSessionVideo();
         //   showTipMessage(sessionLine.getLineName()+": Call established");
            Toast.makeText(getApplicationContext(), "Call established", Toast.LENGTH_SHORT).show();
            statusText.setText("Calling");
        }else{
            sessionLine.reset();
           // showTipMessage(sessionLine.getLineName() + ": failed to answer call !");
            Toast.makeText(getApplicationContext(), "Failed to answer the call", Toast.LENGTH_SHORT).show();

        }

        return rt;
    }

    public void keepCpuRun(boolean keepRun){
        PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        if(keepRun==true){ //open
            if(mCpuLock == null){
                if((mCpuLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SipSampleCpuLock")) == null){
                    return;
                }
                mCpuLock.setReferenceCounted(false);
            }

            synchronized(mCpuLock){
                if(!mCpuLock.isHeld()){
                    mCpuLock.acquire();
                }
            }
        }else{//close
            if(mCpuLock != null){
                synchronized(mCpuLock){
                    if(mCpuLock.isHeld()){
                        //Log.d(TAG,"releasePowerLock()");
                        mCpuLock.release();
                    }
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService(srvIntent);
    }

    @Override
    public void onRegisterSuccess(String s, int i) {
        keepCpuRun(true);
    }

    @Override
    public void onRegisterFailure(String s, int i) {

    }

    @Override
    public void onInviteIncoming(long l, String s, String s1, String s2, String s3, String s4, String s5, boolean b, boolean b1, String s6) {
     //  final Line curSession = new Line(0);
        Ring.getInstance(this).startRingTone();
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Incoming Audio Call");
        alertDialog.setMessage("hello");

        line.setRecvCallState(true);
        line.setSessionId(l);
     //   mSipSdk.muteSession(l, false, false, false, false);
        line.setVideoState(b1);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Answer",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Answer Audio call
                        answerSessionCall(line,false);
                    }
                });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Reject",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Reject call
                        mSipSdk.rejectCall(line.getSessionId(), 486);
                        line.reset();

                      //  showTipMessage("Rejected call");
                        Toast.makeText(getApplicationContext(), "Rejected call", Toast.LENGTH_SHORT).show();

                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onInviteTrying(long l) {

    }

    @Override
    public void onInviteSessionProgress(long l, String s, String s1, boolean b, boolean b1, boolean b2, String s2) {

    }

    @Override
    public void onInviteRinging(long l, String s, int i, String s1) {
        statusText.setText("ringing...");
    }

    @Override
    public void onInviteAnswered(long l, String s, String s1, String s2, String s3, String s4, String s5, boolean b, boolean b1, String s6) {
        Line line = this.line;
        statusText.setText("invite answered");

        line.setVideoState(b1);
        line.setSessionState(true);
        Ring.getInstance(getApplicationContext()).stopRingBackTone();
    }

    @Override
    public void onInviteFailure(long sessionId, String reason, int code) {
        statusText.setText("invite failed");
        line.reset();
     //   Ring.getInstance(getApplicationContext()).stopRingBackTone();
    }

    @Override
    public void onInviteUpdated(long l, String s, String s1, boolean b, boolean b1, String s2) {

    }

    @Override
    public void onInviteConnected(long l) {
        statusText.setText("invite connected");
    }

    @Override
    public void onInviteBeginingForward(String s) {

    }

    @Override
    public void onInviteClosed(long l) {
        statusText.setText("closed");
        Ring.getInstance(getApplicationContext()).stopRingBackTone();
        line.reset();
    }

    @Override
    public void onDialogStateUpdated(String s, String s1, String s2, String s3) {

    }

    @Override
    public void onRemoteHold(long l) {

    }

    @Override
    public void onRemoteUnHold(long l, String s, String s1, boolean b, boolean b1) {

    }

    @Override
    public void onReceivedRefer(long l, long l1, String s, String s1, String s2) {

    }

    @Override
    public void onReferAccepted(long l) {

    }

    @Override
    public void onReferRejected(long l, String s, int i) {

    }

    @Override
    public void onTransferTrying(long l) {

    }

    @Override
    public void onTransferRinging(long l) {

    }

    @Override
    public void onACTVTransferSuccess(long l) {

    }

    @Override
    public void onACTVTransferFailure(long l, String s, int i) {

    }

    @Override
    public void onReceivedSignaling(long l, String s) {

    }

    @Override
    public void onSendingSignaling(long l, String s) {

    }

    @Override
    public void onWaitingVoiceMessage(String s, int i, int i1, int i2, int i3) {

    }

    @Override
    public void onWaitingFaxMessage(String s, int i, int i1, int i2, int i3) {

    }

    @Override
    public void onRecvDtmfTone(long sessionId, int tone) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvOptions(String s) {

    }

    @Override
    public void onRecvInfo(String s) {

    }

    public void onPresenceRecvSubscribe(long subscribeId,
                                        String fromDisplayName, String from, String subject) {}

    @Override
    public void onPresenceOnline(String s, String s1, String s2) {

    }

    @Override
    public void onPresenceOffline(String s, String s1) {

    }

    @Override
    public void onRecvMessage(long l, String s, String s1, byte[] bytes, int i) {

    }

    @Override
    public void onRecvOutOfDialogMessage(String s, String s1, String s2, String s3, String s4, String s5, byte[] bytes, int i) {

    }

    @Override
    public void onSendMessageSuccess(long l, long l1) {

    }

    @Override
    public void onSendMessageFailure(long l, long l1, String s, int i) {

    }

    @Override
    public void onSendOutOfDialogMessageSuccess(long l, String s, String s1, String s2, String s3) {

    }

    @Override
    public void onSendOutOfDialogMessageFailure(long l, String s, String s1, String s2, String s3, String s4, int i) {

    }

    @Override
    public void onPlayAudioFileFinished(long l, String s) {

    }

    @Override
    public void onPlayVideoFileFinished(long l) {

    }

    @Override
    public void onReceivedRTPPacket(long l, boolean b, byte[] bytes, int i) {

    }

    @Override
    public void onSendingRTPPacket(long l, boolean b, byte[] bytes, int i) {

    }

    @Override
    public void onAudioRawCallback(long l, int i, byte[] bytes, int i1, int i2) {

    }

    @Override
    public void onVideoRawCallback(long l, int i, int i1, int i2, byte[] bytes, int i3) {

    }

    @Override
    public void onVideoDecodedInfoCallback(long l, int i, int i1, int i2, int i3) {

    }
}
